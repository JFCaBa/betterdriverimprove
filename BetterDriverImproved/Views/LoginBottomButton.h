//
//  LoginBottomButton.h
//  BetterDriverImproved
//
//  Created by Jose Catala on 04/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface LoginBottomButton : UIButton

@property IBInspectable BOOL topBorder;
@property IBInspectable BOOL bottomBorder;
@property IBInspectable BOOL leftBorder;
@property IBInspectable BOOL rightBorder;

@end
