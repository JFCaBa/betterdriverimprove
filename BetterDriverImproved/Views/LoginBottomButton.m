//
//  LoginBottomButton.m
//  BetterDriverImproved
//
//  Created by Jose Catala on 04/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "LoginBottomButton.h"

@implementation LoginBottomButton


- (void)drawRect:(CGRect)rect
{    
    UIView *bottom = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 1.0f, self.frame.size.width, 1)];
    bottom.backgroundColor = [UIColor whiteColor];
    
    UIView *left = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, self.frame.size.height)];
    left.backgroundColor = [UIColor whiteColor];
    
    UIView *right = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width - 1, 0, 1, self.frame.size.height)];
    right.backgroundColor = [UIColor whiteColor];
    
    UIView *top = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 1)];
    top.backgroundColor = [UIColor whiteColor];
    
    if (_bottomBorder) [self addSubview:bottom];
    
    if (_leftBorder) [self addSubview:left];
    
    if (_rightBorder) [self addSubview:right];
    
    if (_topBorder) [self addSubview:top];
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    [self centerVertically];
}

- (void) setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    
    if (highlighted) {
        self.alpha = 0.5;
    }
    else
    {
        self.alpha = 1;
    }
    
}

- (void)centerVerticallyWithPadding:(float)padding
{
    CGSize imageSize = self.imageView.frame.size;
    CGSize titleSize = self.titleLabel.frame.size;
    CGFloat totalHeight = (imageSize.height + titleSize.height + padding);
    
    self.imageEdgeInsets = UIEdgeInsetsMake(- (totalHeight - imageSize.height),
                                            0.0f,
                                            0.0f,
                                            - titleSize.width);
    
    self.titleEdgeInsets = UIEdgeInsetsMake(0.0f,
                                            - imageSize.width,
                                            - (totalHeight - titleSize.height),
                                            0.0f);
    
    self.contentEdgeInsets = UIEdgeInsetsMake(0.0f,
                                              0.0f,
                                              0.0f,
                                              0.0f);
}

- (void)centerVertically
{
    const CGFloat kDefaultPadding = 3.0f;
    [self centerVerticallyWithPadding:kDefaultPadding];
}
@end
